# pitch-detector-audio-worklet

required reading: the browser AudioWorklet API

description: a web audio worklet that detects the current pitch of an audio stream and sends
messages back periodically with that information.

How to build and deploy: The project is a single typescript file that compiles into a single
javascript file that can be included into a website and then invoked:

```
await context.audioWorklet.addModule('build/pitch-detector.js');
const mediaStream = await navigator.mediaDevices.getUserMedia({audio: true});
const micNode = context.createMediaStreamSource(mediaStream);
const pitchDetectorNode = new AudioWorkletNode(context, 'pitch-detector');
pitchDetectorNode.port.postMessage({sampleRate: context.sampleRate});
pitchDetectorNode.port.postMessage({command: 'start'});
pitchDetectorNode.port.onmessage = ({data}) => {
    console.log(data)
};
micNode.connect(pitchDetectorNode).connect(context.destination);
```

import sampleData from "./test-samples.json"

const UPDATE_INTERVAL = 100

class Peak {

    public constructor(public frequency: number, private lag: number, private strength: number) {
    }

    public toString() {
        return `(${this.frequency}Hz, @${this.strength} strength)`;
    }

    //TODO: work out if this comparator is in the right direction
    public static strengthComparator(peak: Peak, peak2: Peak) {
        if (peak2 < peak) {
            return -1;
        }
        if (peak2 > peak) {
            return 1;
        }
        return 0;
    }
}

class SampledAudio {

    private static NZEROS = 4;
    private static NPOLES = 4;
    private static GAIN = 17.66861134;
    private xv: number[] = [];
    private yv: number[] = [];

    constructor(private samples: Float32Array, private sampleRate: number) {
        this.xv = [0,0,0,0,0]
        this.yv = [0,0,0,0,0]
    }

    public removeDC(): SampledAudio {
        const newSamples: Float32Array = new Float32Array(this.samples.length);
        const dcOffset = this.getAverageValue();
        for (let i = 0; i<this.samples.length; i++) {
            newSamples[i] = this.samples[i] - dcOffset;
        }
        return new SampledAudio(newSamples, this.sampleRate);
    }

/*
    #define NZEROS 4
            #define NPOLES 4
            #define GAIN   1.766861134e+01

    static float xv[NZEROS+1], yv[NPOLES+1];

    static void filterloop()
    { for (;;)
    { xv[0] = xv[1]; xv[1] = xv[2]; xv[2] = xv[3]; xv[3] = xv[4];
        xv[4] = next input value / GAIN;
        yv[0] = yv[1]; yv[1] = yv[2]; yv[2] = yv[3]; yv[3] = yv[4];
        yv[4] =   (xv[0] + xv[4]) - 2 * xv[2]
                + ( -0.4548528197 * yv[0]) + (  2.1363460554 * yv[1])
                + ( -3.9045293216 * yv[2]) + (  3.2230062140 * yv[3]);
        next output value = yv[4];
    }
    }
    */

    public bandpass(): SampledAudio {
        const newSamples = new Float32Array(this.samples.length)
        for (let i=0; i<this.samples.length; i++) {
            this.xv[0] = this.xv[1]; this.xv[1] = this.xv[2]; this.xv[2] = this.xv[3]; this.xv[3] = this.xv[4];
            this.xv[4] = this.samples[i] / SampledAudio.GAIN;
            this.yv[0] = this.yv[1]; this.yv[1] = this.yv[2]; this.yv[2] = this.yv[3]; this.yv[3] = this.yv[4];
            this.yv[4] = (this.xv[0] + this.xv[4]) - 2.0 * this.xv[2]
                    + ( -0.4548528197 * this.yv[0]) + (  2.1363460554 * this.yv[1])
                    + ( -3.9045293216 * this.yv[2]) + (  3.2230062140 * this.yv[3]);
            newSamples[i] = this.yv[4];
        }
        return new SampledAudio(newSamples, this.sampleRate);
    }

    public getAverageValue(): number {
        const total = this.samples.reduce((a,b) => a+b, 0);
        return total / this.samples.length;
    }

    public getRMSValue(): number {
        const squares = this.samples.map((a) => (a*a));
        const sum = squares.reduce((acum, val) => (acum + val));
        const mean = sum/this.samples.length;
        return Math.sqrt(mean);
    }

    public autoCorrelate(): SampledAudio {
        const result = new Float32Array(this.samples.length)
        for (let j = 0; j < this.samples.length; j++) {
            result[j] = 0.0;
            for (let n = 0; n < this.samples.length - j; n++) {
                result[j] += this.samples[n] * this.samples[n+j];
            }
        }
        return new SampledAudio(result, this.sampleRate);
    }

    public nccf(): SampledAudio {
        const result = new Float32Array(this.samples.length)
        let sumofAllSquared = 0.0;
        for (let j=0; j<this.samples.length; j++) {
            sumofAllSquared += this.samples[j] * this.samples[j];
        }
        for (let j = 0; j < this.samples.length; j++) {
            result[j] = 0.0;
            let sumOfRemainingSquared = 0.0;
            for (let n = 0; n < this.samples.length - j; n++) {
                result[j] += this.samples[n] * this.samples[n+j];
                sumOfRemainingSquared += this.samples[n] * this.samples[n];
            }
            result[j] = result[j] / Math.sqrt(sumofAllSquared * sumOfRemainingSquared);

        }
        return new SampledAudio(result, this.sampleRate);
    }

    public movingAverage(): SampledAudio {
        const windowLength = 8;
        const fwl = windowLength;// as a float, in java
        const result = new Float32Array(this.samples.length)
        for (let j = 0; j < this.samples.length; j++) {
            result[j] = 0.0;
            for (let n = 0; n < windowLength && ((n+j) < this.samples.length); n++) {
                result[j] += this.samples[n+j];
            }
            result[j] = result[j] / fwl;
        }
        return new SampledAudio(result, this.sampleRate);
    }

    public calculateAndDisplayZeroCrossingStatistics() {

//        ArrayList<Peak> peaks = new ArrayList<Peak>(findAutocorrelationPeaks());
//        if (peaks.size() == 0) {
//        }
    }

//    private float calculateVariance(ArrayList<Float> samples, float averageDifference) {
//        float variance = 0.0f;
//        for (Float sample : samples) {
//            float distance = sample.floatValue() - averageDifference;
//            variance += Math.sqrt(distance * distance);
//        }
//        return variance / (float)samples.size();
//    }

//    private float calculateAverage(ArrayList<Float> crossingDifferences) {
//        float average = 0.0f;
//        for (Float diff : crossingDifferences) {
//            average += diff.floatValue();
//        }
//        return average / (float)crossingDifferences.size();
//    }

//    private ArrayList<Float> calculateDifferences(ArrayList<Float> crossingPoints) {
//        ArrayList<Float> results = new ArrayList<Float>();
//        for (int i = 0; i < (crossingPoints.size() - 1); i++) {
//            results.add(crossingPoints.get(i+1) - crossingPoints.get(i));
//        }
//        return results;
//    }

    public findAutocorrelationPeaks(): Peak[] {
        const peaks: Peak[] = [];
        for (let i=1; i<(this.samples.length-1); i++) {
            const s = this.samples[i];
            if (s > 0.0 && s > this.samples[i-1] && s > this.samples[i+1]) {
                peaks.push(new Peak(this.sampleRate / i, i, this.samples[i]));
            }
        }
        peaks.sort(Peak.strengthComparator);
        return peaks;
    }

//    private ArrayList<Float> calculateCrossingPoints() {
//        ArrayList<Float> crossingPoints = new ArrayList<Float>();
//        for (int i = 0; i < (samples.length - 1); i++) {
//            if ((samples[i] > 0.0f && samples[i+1] < 0.0f)
//                    || (samples[i] < 0.0f && samples[i+1] > 0.0f)) {
//
//                // naive approach; assume zero crossing is half-way between points
////                crossingPoints.add(((float)i) + 0.5f);
//
//                // slightly more complex approach - fit a line through the zero-point.
//
//                // y = mx + c
//                // y1 = samples[i]; y2 = samples[i+1]
//                // slope = y2-y1 / x2-x1
//                // x2 = 1, x1 = 0
//                // slope (m) = samples[i+1] - samples[i]
//                // at point x=0, height is samples[i], so c = samples[i];
//                // y = (samples[i+1] - samples[i])x + samples[i]
//                // solve for y = 0
//                // 0 = (samples[i+1] - samples[i])x + samples[i];
//                // -samples[i] = (samples[i+1] - samples[i])x
//                // x = -samples[i] / (samples[i+1] - samples[i])
//                crossingPoints.add(((float)i) + Float.valueOf(-samples[i] / (samples[i+1] - samples[i])));
//            }
//        }
//        return crossingPoints;
//    }

    public normalise(): SampledAudio {
        let max = 0.0;
        for (let i=0; i<this.samples.length; i++) {
            const samp = Math.abs(this.samples[i]);
            if (samp > max) {
                max = samp;
            }
        }
        const newSamples = new Float32Array(this.samples.length)
        for (let i=0; i<this.samples.length; i++) {
            newSamples[i] = this.samples[i] / max;
        }
        return new SampledAudio(newSamples, this.sampleRate);
    }
}

const samps = new Float32Array(sampleData.length);
sampleData.map((d, i) => samps[i] = d)
const sampledAudio = new SampledAudio(samps, 48000);
console.log(sampledAudio.getRMSValue());
const peaks = sampledAudio.bandpass().bandpass().removeDC().nccf().findAutocorrelationPeaks()
console.log(`peaks: ${peaks}`)
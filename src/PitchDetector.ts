const UPDATE_INTERVAL = 100

class Peak {

    public constructor(public frequency: number, private lag: number, private strength: number) {
    }

    public toString() {
        return `(${this.frequency}Hz, @${this.strength} strength)`;
    }

    //TODO: work out if this comparator is in the right direction
    public static strengthComparator(peak: Peak, peak2: Peak) {
        if (peak2 < peak) {
            return -1;
        }
        if (peak2 > peak) {
            return 1;
        }
        return 0;
    }
}

class SampledAudio {

    private static NZEROS = 4;
    private static NPOLES = 4;
    private static GAIN = 17.66861134;
    private xv: number[] = [];
    private yv: number[] = [];

    constructor(private samples: Float32Array, private sampleRate: number) {
        this.xv = [0,0,0,0,0]
        this.yv = [0,0,0,0,0]
    }

    public removeDC(): SampledAudio {
        const newSamples: Float32Array = new Float32Array(this.samples.length);
        const dcOffset = this.getAverageValue();
        for (let i = 0; i<this.samples.length; i++) {
            newSamples[i] = this.samples[i] - dcOffset;
        }
        return new SampledAudio(newSamples, this.sampleRate);
    }

    public bandpass(): SampledAudio {
        const newSamples = new Float32Array(this.samples.length)
        for (let i=0; i<this.samples.length; i++) {
            this.xv[0] = this.xv[1]; this.xv[1] = this.xv[2]; this.xv[2] = this.xv[3]; this.xv[3] = this.xv[4];
            this.xv[4] = this.samples[i] / SampledAudio.GAIN;
            this.yv[0] = this.yv[1]; this.yv[1] = this.yv[2]; this.yv[2] = this.yv[3]; this.yv[3] = this.yv[4];
            this.yv[4] = (this.xv[0] + this.xv[4]) - 2.0 * this.xv[2]
                    + ( -0.4548528197 * this.yv[0]) + (  2.1363460554 * this.yv[1])
                    + ( -3.9045293216 * this.yv[2]) + (  3.2230062140 * this.yv[3]);
            newSamples[i] = this.yv[4];
        }
        return new SampledAudio(newSamples, this.sampleRate);
    }

    public getAverageValue(): number {
        const total = this.samples.reduce((a,b) => a+b, 0);
        return total / this.samples.length;
    }

    public getRMSValue(): number {
        const squares = this.samples.map((a) => (a*a));
        const sum = squares.reduce((acum, val) => (acum + val));
        const mean = sum/this.samples.length;
        return Math.sqrt(mean);
    }

    public autoCorrelate(): SampledAudio {
        const result = new Float32Array(this.samples.length)
        for (let j = 0; j < this.samples.length; j++) {
            result[j] = 0.0;
            for (let n = 0; n < this.samples.length - j; n++) {
                result[j] += this.samples[n] * this.samples[n+j];
            }
        }
        return new SampledAudio(result, this.sampleRate);
    }

    public nccf(): SampledAudio {
        const result = new Float32Array(this.samples.length)
        let sumofAllSquared = 0.0;
        for (let j=0; j<this.samples.length; j++) {
            sumofAllSquared += this.samples[j] * this.samples[j];
        }
        for (let j = 0; j < this.samples.length; j++) {
            result[j] = 0.0;
            let sumOfRemainingSquared = 0.0;
            for (let n = 0; n < this.samples.length - j; n++) {
                result[j] += this.samples[n] * this.samples[n+j];
                sumOfRemainingSquared += this.samples[n] * this.samples[n];
            }
            result[j] = result[j] / Math.sqrt(sumofAllSquared * sumOfRemainingSquared);

        }
        return new SampledAudio(result, this.sampleRate);
    }

    public movingAverage(): SampledAudio {
        const windowLength = 8;
        const fwl = windowLength;// as a float, in java
        const result = new Float32Array(this.samples.length)
        for (let j = 0; j < this.samples.length; j++) {
            result[j] = 0.0;
            for (let n = 0; n < windowLength && ((n+j) < this.samples.length); n++) {
                result[j] += this.samples[n+j];
            }
            result[j] = result[j] / fwl;
        }
        return new SampledAudio(result, this.sampleRate);
    }

    public findAutocorrelationPeaks(): Peak[] {
        const peaks: Peak[] = [];
        for (let i=1; i<(this.samples.length-1); i++) {
            const s = this.samples[i];
            if (s > 0.0 && s > this.samples[i-1] && s > this.samples[i+1]) {
                peaks.push(new Peak(this.sampleRate / i, i, this.samples[i]));
            }
        }
        peaks.sort(Peak.strengthComparator);
        return peaks;
    }

    public normalise(): SampledAudio {
        let max = 0.0;
        for (let i=0; i<this.samples.length; i++) {
            const samp = Math.abs(this.samples[i]);
            if (samp > max) {
                max = samp;
            }
        }
        const newSamples = new Float32Array(this.samples.length)
        for (let i=0; i<this.samples.length; i++) {
            newSamples[i] = this.samples[i] / max;
        }
        return new SampledAudio(newSamples, this.sampleRate);
    }
}

class PitchDetector extends AudioWorkletProcessor {
    
    private running: boolean
	private lastUpdate: number
	private pitch: number
	private sampleRate: number

	constructor() {
		super();
        this.running = false
		this.pitch = 0
		this.sampleRate = 16384
		this.lastUpdate = new Date().getTime();
		if (this.port) {
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			this.port.onmessage = (e) => {
                if (e.data.command === 'start') {
                    this.running = true;
                }
                else if (e.data.command === 'stop') {
                    this.running = false;
                }
                else if (e.data.sampleRate) {
                    this.sampleRate = e.data.sampleRate;
                }
            }
		}
	}

	process(inputs: Float32Array[][]) {

        if (this.running) {
            const inputChannelData = inputs[0][0]
            const sampledAudio = new SampledAudio(inputChannelData, this.sampleRate)
            const peaks = sampledAudio.bandpass().bandpass().removeDC().nccf().findAutocorrelationPeaks()
            if (peaks.length > 0) {
                this.pitch = peaks[0].frequency
            }
            const currentTime = new Date().getTime()
            // Post a message to the node every 16ms.
            if (currentTime - this.lastUpdate > UPDATE_INTERVAL) {
                this.port.postMessage({ pitch: this.pitch, loudness: sampledAudio.getRMSValue() })
                this.lastUpdate = currentTime
            }
        }

		return true;
	}
}

registerProcessor("pitch-detector", PitchDetector);